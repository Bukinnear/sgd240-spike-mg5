---
title: Spike Report
---

MG 5 – Session-Based Multiplayer
================================

Introduction
------------

In previous projects we have learnt how to join a server directly with
an IP address using UGameplayStatics::OpenLevel()

Now we would like to know how to create, find, and join a game using a
session-based system in C++

Goals
-----

-   A simple menu to either host, or search for an open multiplayer game
    over a LAN network

    -   These games should rely on sessions to achieve this

    -   Any session-based code should be done within c++

-   Once created, or joined, players should be placed in the same
    default map, and at least be able to see each other moving.

> Answer the following:

-   Skill: How to create, search for, and join session-based multiplayer
    games.

-   Knowledge: What are sessions, and what do they do in Unreal 4’s
    networking systems?

Personnel
---------

In this section, list the primary author of the spike report, as well as
any personnel who assisted in completing the work.

  ------------------- -----------------
  Primary – Jared K   Secondary – N/A
  ------------------- -----------------

Technologies, Tools, and Resources used
---------------------------------------

-   Unreal Docs – Sessions + Matchmaking
    <https://docs.unrealengine.com/latest/INT/Programming/Online/Interfaces/Session>

-   How to Use Sessions in C++ (Important)
    <https://wiki.unrealengine.com/How_To_Use_Sessions_In_C%2B%2B#Getting_Started>

-   How to use Sessions in Blueprint (Shootout example game)
    <https://docs.unrealengine.com/latest/INT/Resources/Showcases/BlueprintMultiplayer>

-   How to use Delegates in C++
    <https://docs.unrealengine.com/latest/INT/Programming/UnrealArchitecture/Delegates/#executingdelegates>

-   What to do when you cannot \#include “Online.h”
    <https://forums.unrealengine.com/development-discussion/c-gameplay-programming/21670-can-t-include-online-h-even-after-altering-build-cs>

Tasks Undertaken
----------------

1.  Follow the [How to use Sessions in
    C++Tutorial](https://wiki.unrealengine.com/How_To_Use_Sessions_In_C%2B%2B#Getting_Started)

    a.  Note: This tutorial is made for 4.7, but the only change that
        needs to be made to accommodate 4.15 is when declaring the
        constructor, it no longer takes arguments, and it doesn’t need
        to reference the super in the cpp file.

2.  Add a menu widget to call the functions added in the above, I used a
    separate map, though I don’t think it is strictly required.

    a.  **Note**: The tutorial’s map names may be different to yours,
        watch for that in the creating and destroying sections

What We Found Out
-----------------

### Sessions and the Online Subsystem

1.  Sessions are Unreal’s way of providing discovery, and connecting of
    game clients to servers.

2.  A host will create a session, with a specific collection of
    settings, and (if the settings allow) other clients can find and
    join that session.

3.  A game will typically only have 1 session created at any moment, and
    will only ever use 1 session at any given point in time, with some
    exceptions such as some dedicated servers which is outside the scope
    of this project.

    a.  For this reason, when a session ends, and is no longer needed,
        it is important to fully destroy the session to prevent
        confusion and problems in the future.

4.  Sessions are almost entirely managed by the Online Subsystem, which
    is an interface that exposes the functionality from one of several
    only service platforms (such as Steam, iOS, or Xbox Live) to the
    game code in generic format.

    a.  This means that no matter what platform you are targeting, the
        function calls remain the same.

5.  In this particular example, we are using the “Null” Online
    Subsystem, which is Unreals’s subsystem for supporting local LAN
    play.

    a.  This system will only work in a LAN, it will not work over the
        internet without a different service like steam.

### General Notes & Observations

1.  Several of the functions provided by the IOnlineSubsystem interface
    take some time to complete (such as find session, or join session) –
    to counter this, it also provides events that trigger on completion.

    a.  When subscribing to events, we use 2 objects:

        i.  The delegate (which is like declaring a “type” of function)

        ii. The delegate handle (which is a variable of a delegate
            “type”)

            1.  Note that this does not include the declaration or
                definition of an event itself, only the subscription to
                one. More on that can be found
                [here](https://docs.unrealengine.com/latest/INT/Programming/UnrealArchitecture/Delegates/#executingdelegates).

### Setup

1.  For this project, we will be using the Third Person Template.

    a.  The only custom classes you will need are a GameInstance, and a
        menu to call the functions we define.

2.  3 things need to happen before we can use sessions.

    a.  The Online Subsystem type needs to be specified in the
        DefaultEngine.ini file

    b.  Online Subsystem, Subsystem Utils, and the Subsystem type need
        to be specified in the YourProjectName.Build.cs file

    c.  Engine.h, UnrealNetwork.h, and Online.h need to be included in
        the YourProjectName.h file.

### Creating a Session

**Note**: I shall use this section to set a precedent for the following
sections. There should be less code details following this.

1.  We can get the Online Subsystem using the static method

    OnlineSub = IOnlineSubsystem::Get()

2.  We can then access the Subsystem Interface using

    SessionInterface = OnlineSub-&gt;GetSessionInterface()

3.  We first want to use the interface to subscribe to the event fired

    SessionInterface -&gt;AddOnCreateSessionCompleteDelegate\_Handle()

4.  And we will need to call

    Sessions-&gt;CreateSession()

5.  This takes a FOnlineSessionSettings whose settings will need to be
    filled out.

    a.  The map name can be set using

        SessionSettingsObject-&gt;Set(SETTING\_MAPNAME,
        FString("MapName"),
        EOnlineDataAdvertisementType::ViaOnlineService);

6.  When the this finishes, and the event we are subscribed to fires, we
    deregister our

    OnCreateSessionCompleteDelegate

    a.  **Note**: We do this because we may want to repeat this, and
        differently. If we do, we will end up doubling up on registering
        our delegates, and that would get messy

7.  Then we subscribe to the next event,

    SessionInterface-&gt;AddOnStartSessionCompleteDelegate\_Handle()

8.  Then we want to use the interface to

    SessionInterface-&gt;StartSession()

9.  When the next delegate fires, we want to deregister the last event.

10. Lastly, we open the map using

    UGameplayStatics::OpenLevel(GetWorld(), "MapName", true, "listen");

### Finding a Session

1.  Get the online subsystem, and interface pointer

2.  We use a FOnlineSessionSearch object to specify the settings to use.

    a.  Check the source for macros that can be used to alter settings
        using

        QuerySettings.Set()

3.  We then convert that to a TSharedRef&lt;FOnlineSessionSearch&gt;
    object and use it when we call the interface’s

    SessionInterface-&gt;FindSessions()

4.  Remember to subscribe to the appropriate event, and deregister it
    when it has finished

5.  When that finishes, you can find the search results in
    SessionSearch-&gt;SearchResults

### Joining a Session

1.  Use the interface to call

    SessionInterface-&gt;JoinSession()

2.  Subscribe/deregister events before + after calling

3.  Once the join is complete, we can join the match by using

    PlayerController-&gt;ClientTravel()

4.  This takes a URL that we can find using

    a.  SessionInterface-&gt;GetResolvedConnectString()

### Destroying a Session

1.  Sessions-&gt;DestroySession()

2.  You can subscribe to the event fired on completion to open a
    different map when this has finished.

3.  Remember to deregister the event.

### Using These Functions

1.  According to the tutorial, these functions cannot be made Blueprint
    callable, (though I haven’t tested that myself) so we make another
    set of functions designed to call these ones.

\[Optional\] Open Issues/risks
------------------------------

 \[Optional\] Recommendations
-----------------------------

1.  This project makes use of quite a few delegates. If extensive
    modifications were needed for the provided code, it would be wise to
    understand them to a reasonable degree.
