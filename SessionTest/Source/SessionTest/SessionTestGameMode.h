// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "SessionTestGameMode.generated.h"

UCLASS(minimalapi)
class ASessionTestGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASessionTestGameMode();
};



